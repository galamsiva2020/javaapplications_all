package com.durgasoft.service;

import java.io.IOException;

import org.apache.axis.AxisFault;

public class TestClient {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		java.net.URL endpointUrl = new java.net.URL("http://localhost:8080/CalculatorService/services/CalService");
		
		org.apache.axis.client.Service service=
				new org.apache.axis.client.Service();
		
		CalServiceSoapBindingStub  stub = new 
				CalServiceSoapBindingStub(endpointUrl, service);
		 int result= stub.add(10, 20);
		 System.out.println("ADD RESULT "+ result);
		 
		int result2= stub.sub(40, 14);
		System.out.println("SUB RESULT "+ result2);
		

	}

}
